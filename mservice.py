#!/usr/bin/python3

import gi, os, subprocess, locale, sys
gi.require_version("Gtk", "3.0")
from mlocale import translate
from gi.repository import Gtk, GdkPixbuf, Gio, GLib


class MService(Gtk.ApplicationWindow):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)

		self.set_title(_[25])
		self.set_default_size(500,500)

		box = Gtk.VBox()
		self.add(box)

		self.services_black_list = ["checkfs","swap","static-nodes","mountfs","kmods","killer","tunefs","hostname","random"]
		self.set_icon(GdkPixbuf.Pixbuf.new_from_file("/usr/milis/mservice/icons/mservice.svg"))

		self.search_widget = Gtk.SearchEntry()
		self.search_widget.connect("search-changed",self.search_change)
		box.pack_start(self.search_widget,False,False,5)

		self.services_store = Gtk.ListStore(GdkPixbuf.Pixbuf(),str,str)
		self.services_tree = Gtk.TreeView(model=self.services_store)
		self.services_tree.set_activate_on_single_click(True)
		self.services_tree.connect("button-release-event",self.click_tree)

		tree_icon = Gtk.CellRendererPixbuf()
		tree_icon.set_fixed_size(32,32)
		icon = Gtk.TreeViewColumn(_[1],tree_icon,gicon = 0)
		self.services_tree.append_column(icon)

		tree_text = Gtk.CellRendererText()
		text = Gtk.TreeViewColumn(_[2],tree_text, text = 1)
		self.services_tree.append_column(text)

		tree_text = Gtk.CellRendererText()
		text = Gtk.TreeViewColumn(_[24],tree_text, text = 2)
		self.services_tree.append_column(text)

		scroll = Gtk.ScrolledWindow()
		scroll.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
		scroll.add(self.services_tree)
		box.pack_start(scroll,True, True, 5)

		self.update_ui()

	def run_command(self,arg_1,arg_2=False):
		try:
			_timeout = 5
			my_env = os.environ.copy()
			my_env["init_color"] ="0"
			if not arg_2:
				get = subprocess.check_output(["/usr/milis/bin/servis",arg_1],env=my_env,timeout=_timeout)#,stdout = subprocess.PIPE)
			else:
				get = subprocess.check_output(["/usr/milis/bin/servis",arg_1,arg_2],env=my_env,timeout=_timeout)#,stdout = subprocess.PIPE)
			return get.decode("utf-8")
		except:
			return _[19]

	def search_change(self,search_widget):
		search_text = search_widget.get_text()
		s = {}
		for service in self.services.keys():
			if search_text in service:
				s[service] = self.services[service]
		self.append_tree(s)

	def click_tree(self,widget,event):
		selected = self.services_tree.get_selection()
		tree_model, tree_iter = selected.get_selected()
		select = tree_model[tree_iter][1]
		status = self.services[select]
		if event.button == 1 or event.button == 3:
			menu = Gtk.Menu()
			if select in self.e_services:
				rm_service = Gtk.ImageMenuItem(_[21])
				image = Gtk.Image(stock=Gtk.STOCK_REMOVE)
				rm_service.set_image(image)
				rm_service.connect("activate",self.remove_service,select)
				menu.add(rm_service)
				if self.services[select][0]:
					disable = Gtk.ImageMenuItem(_[7])
					image = Gtk.Image(stock=Gtk.STOCK_MEDIA_STOP)
					disable.set_image(image)
					disable.connect("activate",self.disable_service,select)
					menu.add(disable)
				else:
					enable = Gtk.ImageMenuItem(_[6])
					image = Gtk.Image(stock=Gtk.STOCK_MEDIA_PLAY)
					enable.set_image(image)
					enable.connect("activate",self.enable_service,select)
					menu.add(enable)
				if select in self.a_services:
					as_disable = Gtk.ImageMenuItem(_[18])
					image = Gtk.Image(stock=Gtk.STOCK_QUIT)
					as_disable.set_image(image)
					as_disable.connect("activate",self.as_disable,select)
					menu.add(as_disable)
				else:
					as_enable = Gtk.ImageMenuItem(_[17])
					image = Gtk.Image(stock=Gtk.STOCK_REFRESH)
					as_enable.set_image(image)
					as_enable.connect("activate",self.as_enable,select)
					menu.add(as_enable)
			else:
				dd_service = Gtk.ImageMenuItem(_[20])
				image = Gtk.Image(stock=Gtk.STOCK_ADD)
				dd_service.set_image(image)
				dd_service.connect("activate",self.add_service,select)
				menu.add(dd_service)
			menu.show_all()
			menu.popup_at_pointer()

	def update_ui(self):
		self.get_services()
		self.get_autostart_services()
		self.get_e_services()
		self.search_widget.set_text("")
		self.append_tree(self.services)

	def dialog_info(self,title,text):
		info = Gtk.MessageDialog(self,0,Gtk.MessageType.INFO, Gtk.ButtonsType.OK,title)
		info.set_title(title)
		info.format_secondary_text(text)
		info.run()
		info.destroy()
		self.update_ui()

	def add_service(self,widget,service):
		question = Gtk.MessageDialog(self,0,Gtk.MessageType.QUESTION, Gtk.ButtonsType.OK_CANCEL,_[5])
		question.set_title(_[5])
		question.format_secondary_text(_[22].format(service))
		get = question.run()
		if get == Gtk.ResponseType.OK:
			self.run_command("ekle",service)
			self.update_ui()
		question.destroy()

	def remove_service(self,widget,service):
		question = Gtk.MessageDialog(self,0,Gtk.MessageType.QUESTION, Gtk.ButtonsType.OK_CANCEL,_[5])
		question.set_title(_[5])
		question.format_secondary_text(_[23].format(service))
		get = question.run()
		if get == Gtk.ResponseType.OK:
			print("Servis Silindi")
			self.run_command("sil",service)
			self.update_ui()
		question.destroy()

	def enable_service(self,widget,service):
		get = self.run_command("kos",service)
		self.dialog_info(_[8].format(service),get)

	def disable_service(self,widget,service):
		get = self.run_command("dur",service)
		self.dialog_info(_[9].format(service),get)

	def as_disable(self,widget,service):
		get = self.run_command("pasif",service)
		self.dialog_info(_[16].format(service),get)

	def as_enable(self,widget,service):
		get = self.run_command("aktif",service)
		self.dialog_info(_[15].format(service),get)

	def get_services(self):
		self.services = {}
		services = self.run_command("dliste")
		services = services.split("\n")
		for s in services:
			if s and s not in self.services_black_list:
				self.services[s] = self.get_service_status(s)
		self.append_tree(self.services)

	def get_service_status(self,service):
		get = self.run_command("bil",service)
		get = get.split("\n")
		for g in get:
			if "[+]" in g or "[OK]" in g:
				g = g.replace("[OK]","").replace("[+]","")
				g = g.replace("  *   Status ","")
				return (True ,g)
			elif "[X]" in g or "[FAIL]" in g:
				g = g.replace("[FAIL]","").replace("[X]","")
				g = g.replace("  *   Status ","")
				return (False ,g)
		return (False,"")

	def get_autostart_services(self):
		a_services = self.run_command("liste")
		a_services = a_services.split("\n")
		self.a_services = a_services

	def get_e_services(self):
		e_services = self.run_command("eliste")
		e_services = e_services.split("\n")
		self.e_services = e_services

	def append_tree(self,services):
		self.services_store.clear()
		keys_list = list(services.keys())
		keys_list.sort()
		for service in keys_list:
			service_state = services[service][0]
			service_info = services[service][1]
			if service_state:
				icon = GdkPixbuf.Pixbuf.new_from_file("/usr/milis/mservice/icons/2.svg")
			else:
				icon = GdkPixbuf.Pixbuf.new_from_file("/usr/milis/mservice/icons/1.svg")
			self.services_store.append([icon,service,service_info])

class Application(Gtk.Application):
	def __init__(self, *args, **kwargs):
		super().__init__(*args, application_id="mls.akdeniz.mservice", flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE, **kwargs)
		GLib.set_application_name("mservice")
		GLib.set_prgname('mservice')
		self.win = None

	def do_start_up(self):
		Gtk.Application.do_startup(self)

	def do_activate(self):
		if not self.win:
			self.win = MService(application=self)
			self.win.show_all()
		self.win.present()

	def do_command_line(self, command_line):
		if self.win == None:
			self.do_activate()
		self.activate()
		return False

if __name__ == "__main__":
	l = locale.getdefaultlocale()
	l = l[0].split("_")[0]
	locales = list(translate.translate.keys())
	if l not in locales:
		l = "en"
	_ = translate.translate[l]
	if os.getuid() != 0:
		dialog = Gtk.MessageDialog(None,0, Gtk.MessageType.ERROR,
		Gtk.ButtonsType.OK, _[11])
		dialog.format_secondary_text(_[12])
		dialog.run()
	else:
		app = Application()
		app.run(sys.argv)

