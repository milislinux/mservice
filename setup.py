#!/usr/bin/python3

import os
from setuptools import setup, find_packages

#os.makedirs("/usr/share/mservice/icons")

datas = [("/usr/share/applications",["mservice_data/mservice.desktop"]),
  		("/usr/share/icons/hicolor/32x32/apps", ["icons/mservice.png"]),
   		("/usr/share/icons/hicolor/scalable/apps",["icons/mservice.svg"]),
   		("/usr/bin",["mservice_data/mservice-pkexec"]),
   		("/usr/share/polkit-1/actions",["mservice_data/com.milis.pkexec.mservice.policy"]),
   		("/usr/share/mservice/icons",["icons/1.svg","icons/2.svg"])]


setup(
   	name = "mservice",
   	scripts = ["mservice.py"],
   	packages = find_packages(),
   	version = "0.1",
   	description = "Milis Service Manager GTK Frontend",
   	author = ["Fatih Kaya"],
   	author_email = "sonakinci41@gmail.com",
   	url = "https://mls.akdeniz.edu.tr/git/milislinux/mservice",
   	data_files = datas
)
